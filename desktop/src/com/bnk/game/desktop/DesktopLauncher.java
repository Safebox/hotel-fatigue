package com.bnk.game.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.bnk.game.HotelFatigue;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 240;
		config.height = 160;
		config.foregroundFPS = 30;
		config.backgroundFPS = 30;
		config.addIcon("Icon128.png", Files.FileType.Internal);
		config.addIcon("Icon32.png", Files.FileType.Internal);
		config.addIcon("Icon16.png", Files.FileType.Internal);
		new LwjglApplication(new HotelFatigue(), config);
	}
}
