package com.bnk.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;

public class Assets
{
    public static Texture room;
    public static Texture iconSleep;
    public static Texture iconFood;
    public static Texture iconShower;
    public static Texture iconPC;
    public static Pixmap barSleep;
    public static Pixmap barFood;
    public static Pixmap barShower;
    public static Pixmap barPC;

    public static Texture shower;
    public static Texture player;

    public static void Init()
    {
        room = new Texture("room.png");
        iconSleep = new Texture("icons/bed.png");
        iconFood = new Texture("icons/stomach.png");
        iconShower = new Texture("icons/shower.png");
        iconPC = new Texture("icons/pc.png");
        barSleep = new Pixmap(16, 112, Pixmap.Format.RGBA8888);
        barFood = new Pixmap(16, 112, Pixmap.Format.RGBA8888);
        barShower = new Pixmap(16, 112, Pixmap.Format.RGBA8888);
        barPC = new Pixmap(16, 112, Pixmap.Format.RGBA8888);

        shower = new Texture("shower.png");
        player = new Texture("player.png");
    }

    public static void UpdateBar(Pixmap pm, byte v)
    {
        pm.setColor(Color.CLEAR);
        pm.fill();
        pm.setColor(Color.valueOf("ff0044"));
        pm.fillRectangle(0,pm.getHeight() - 1 - MathUtils.round((v + Byte.MAX_VALUE) / 255f * pm.getHeight()),pm.getWidth(), MathUtils.round((v + Byte.MAX_VALUE) / 255f * pm.getHeight()));
    }

    public static void Dispose()
    {
        room.dispose();
        iconSleep.dispose();
        iconFood.dispose();
        iconShower.dispose();
        iconPC.dispose();
        barSleep.dispose();
        barFood.dispose();
        barShower.dispose();
        barPC.dispose();

        shower.dispose();
        player.dispose();
    }
}