package com.bnk.game;

import com.badlogic.gdx.math.MathUtils;

public class Player
{
    private static int activity;
    private static byte[] needs;

    public static void Init()
    {
        activity = 0;
        needs = new byte[]{0, 0, 0, 0};
    }

    public static int GetActivity()
    {
        return activity;
    }

    public static void SetActivity(int a)
    {
        activity = a;
    }

    public static byte[] GetNeeds()
    {
        return needs;
    }

    public static void SetNeeds(byte... n)
    {
        for (int i = 0; i < n.length; i++)
        {
            needs[i] = n[i];
        }
    }

    public static void AddNeeds(byte... n)
    {
        for (int i = 0; i < n.length; i++)
        {
            needs[i] = (byte)MathUtils.clamp((int)needs[i] + n[i], Byte.MIN_VALUE, Byte.MAX_VALUE);
        }
    }
}