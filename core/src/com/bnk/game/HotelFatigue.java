package com.bnk.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class HotelFatigue extends ApplicationAdapter
{
    SpriteBatch batch;
    OrthographicCamera cam;
    FitViewport view;

    @Override
    public void create()
    {
        batch = new SpriteBatch();
        cam = new OrthographicCamera();
        view = new FitViewport(240, 160, cam);
        view.apply();
        Assets.Init();
        Player.Init();
    }

    @Override
    public void render()
    {
        Color c = Color.valueOf("181425");
        Gdx.gl.glClearColor(c.r, c.g, c.b, c.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //Update
        if (Player.GetNeeds()[0] >= 102 && Player.GetNeeds()[1] >= 102 && Player.GetNeeds()[2] >= 102 && Player.GetNeeds()[3] >= 102)
        {
            Gdx.net.openURI("https://www.youtube.com/watch?v=S22_DuaoHCU");
            System.exit(0);
        }
        cam.update();
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_1))
        {
            Player.SetActivity(0);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_2))
        {
            Player.SetActivity(1);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_3))
        {
            Player.SetActivity(2);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_4))
        {
            Player.SetActivity(3);
        }
        switch (Player.GetActivity())
        {
            case 0:
                Player.AddNeeds((byte)2, (byte)-1, (byte)-1, (byte)0);
                break;
            case 1:
                Player.AddNeeds((byte)-1, (byte)2, (byte)0, (byte)-1);
                break;
            case 2:
                Player.AddNeeds((byte)0, (byte)-1, (byte)2, (byte)-1);
                break;
            case 3:
                Player.AddNeeds((byte)-1, (byte)0, (byte)-1, (byte)2);
                break;
        }
        Assets.UpdateBar(Assets.barSleep, Player.GetNeeds()[0]);
        Assets.UpdateBar(Assets.barFood, Player.GetNeeds()[1]);
        Assets.UpdateBar(Assets.barShower, Player.GetNeeds()[2]);
        Assets.UpdateBar(Assets.barPC, Player.GetNeeds()[3]);

        //Render
        batch.setProjectionMatrix(cam.combined);
        batch.begin();
        //Room
        batch.draw(Assets.room, 8, cam.viewportHeight / 2 - Assets.room.getHeight() / 2);
        //Icons
        batch.draw(Assets.iconSleep, cam.viewportWidth / 2 - 16, 8);
        batch.draw(Assets.iconFood, cam.viewportWidth / 2 + 16, 8);
        batch.draw(Assets.iconShower, cam.viewportWidth / 2 + 48, 8);
        batch.draw(Assets.iconPC, cam.viewportWidth / 2 + 80, 8);
        //Bars
        batch.draw(new Texture(Assets.barSleep), cam.viewportWidth / 2 - 8, 40);
        batch.draw(new Texture(Assets.barFood), cam.viewportWidth / 2 + 24, 40);
        batch.draw(new Texture(Assets.barShower), cam.viewportWidth / 2 + 56, 40);
        batch.draw(new Texture(Assets.barPC), cam.viewportWidth / 2 + 88, 40);
        //Overlays
        switch (Player.GetActivity())
        {
            case 0:
                batch.draw(Assets.player, 8, cam.viewportHeight / 2 + 8, (int)(16 * Math.floor((System.currentTimeMillis() / 500d) % 2)), 0, 16, 16);
                break;
            case 1:
                batch.draw(Assets.player, 8 + (16 * 3.5f), 8 + 32, (int)(16 * Math.floor((System.currentTimeMillis() / 500d) % 2)), 16, 16, 16);
                break;
            case 2:
                batch.draw(Assets.shower,  cam.viewportWidth / 2 - 64, 72, 32, 32, 32, 32);
                break;
            case 3:
                batch.draw(Assets.player, 8, 8 + 32, 0, 32 + (int)(32 * Math.floor((System.currentTimeMillis() / 500d) % 2)), 32, 32);
                break;
        }
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {
        super.resize(width, height);
        view.update(width, height);
        cam.position.set(cam.viewportWidth / 2, cam.viewportHeight / 2, 0);
    }

    @Override
    public void dispose()
    {
        batch.dispose();
        Assets.Dispose();
    }
}
